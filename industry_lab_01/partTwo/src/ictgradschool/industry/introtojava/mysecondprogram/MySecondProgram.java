package ictgradschool.industry.introtojava.mysecondprogram;

public class MySecondProgram {
    public void start() {
        System.out.println("Hello World");
    }

    public static void main(String[] args) {
        MySecondProgram p = new MySecondProgram();
        p.start();
    }
}
